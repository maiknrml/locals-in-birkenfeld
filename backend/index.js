const express = require("express");
const bodyParser = require("body-parser");
const app = express();
const cors = require("cors");
const mongodb = require("mongodb");
const formidable = require("formidable");
const path = require("path");
const fs = require("fs");
const throttle = require("express-throttle-bandwidth");
const multer = require("multer");
const ejs = require("ejs");

const port = process.env.PORT || 3000;

app.use(cors());

app.use(bodyParser.json());

const storage = multer.diskStorage({
  destination: "./uploads"
});

const upload = multer({
  storage: storage
}).single("myimage");

app.post("/upload", upload, function(req, res, next) {
  if (err) {
    console.log(err);
  } else {
    console.log(req.file.originalname);
    res.send("Uploaded");
  }
});

app.listen(port, () => {
  console.log(" Server running on http://localhost:" + port);
});
