import { RouteConfig } from "vue-router";

const routes: RouteConfig[] = [
  {
    path: "/",
    component: () => import("layouts/MainLayout.vue"),
    children: [
      { path: "", component: () => import("src/views/Home.vue") },
      { path: "/übersicht", component: () => import("../views/Übersicht.vue") },
      { path: "/kontakt", component: () => import("../views/Kontakt.vue") },
      { path: "/login", component: () => import("../views/Login.vue") },
      {
        path: "/registrieren",
        component: () => import("../views/Register.vue")
      },
      {
        path: "/datenschutz",
        component: () => import("../views/Datenschutz.vue")
      }
    ]
  }
];

// Always leave this as last one
if (process.env.MODE !== "ssr") {
  routes.push({
    path: "*",
    component: () => import("../views/Error404.vue")
  });
}

export default routes;
